/******************************************************************************
  * @file    lora.c
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   lora API to drive the lora state Machine
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"
#include "timeServer.h"
#include "LoRaMac.h"
#include "lora.h"
#include "lora-test.h"

/*!
 * Join requests trials duty cycle.
 */
#define OVER_THE_AIR_ACTIVATION_DUTYCYCLE           10000  // 10 [s] value in ms

#if defined( REGION_EU868 )

#include "LoRaMacTest.h"

#define USE_SEMTECH_DEFAULT_CHANNEL_LINEUP          0

#if( USE_SEMTECH_DEFAULT_CHANNEL_LINEUP == 1 )

#define LC4                { 867100000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC5                { 867300000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC6                { 867500000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC7                { 867700000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC8                { 867900000, 0, { ( ( DR_5 << 4 ) | DR_0 ) }, 0 }
#define LC9                { 868800000, 0, { ( ( DR_7 << 4 ) | DR_7 ) }, 2 }
#define LC10               { 868300000, 0, { ( ( DR_6 << 4 ) | DR_6 ) }, 1 }

#endif

#endif

/* LoRa SPI device structure */
static SPI_HandleTypeDef* LoRaSPI;

static MlmeReqJoin_t JoinParameters;

const volatile static lora_Configuration_t* LoRaConfig;
const volatile static LoRaParam_t* LoRaParam;
static LoRaMacPrimitives_t LoRaMacPrimitives;
static LoRaMacCallback_t LoRaMacCallbacks;
static MibRequestConfirm_t mibReq;

static LoRaMainCallback_t* LoRaMainCallbacks;
/*!
 * \brief   MCPS-Confirm event function
 *
 * \param   [IN] McpsConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void McpsConfirm(McpsConfirm_t* mcpsConfirm)
{
  if (mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {
    switch (mcpsConfirm->McpsRequest) {
    case MCPS_UNCONFIRMED:
      // Check Datarate
      // Check TxPower
      break;

    case MCPS_CONFIRMED:
      if (LoRaMainCallbacks->LORA_Confirmation != NULL) {

        // Check Datarate
        // Check TxPower
        // Check AckReceived
        // Check NbTrials
        (*LoRaMainCallbacks->LORA_Confirmation)(mcpsConfirm->UpLinkCounter, mcpsConfirm->AckReceived);
      }
      break;

    case MCPS_PROPRIETARY:
      break;

    default:
      break;
    }
  } else if ((mcpsConfirm->Status == LORAMAC_EVENT_INFO_STATUS_RX2_TIMEOUT) && (mcpsConfirm->McpsRequest == MCPS_CONFIRMED)) {
    if (LoRaMainCallbacks->LORA_Confirmation != NULL) {
      (*LoRaMainCallbacks->LORA_Confirmation)(mcpsConfirm->UpLinkCounter, mcpsConfirm->AckReceived);
    }
  }
}

/*!
 * \brief   MCPS-Indication event function
 *
 * \param   [IN] mcpsIndication - Pointer to the indication structure,
 *               containing indication attributes.
 */
static void McpsIndication(McpsIndication_t* mcpsIndication)
{
  lora_AppData_t AppData;
  if (mcpsIndication->Status != LORAMAC_EVENT_INFO_STATUS_OK) {
    return;
  }

  switch (mcpsIndication->McpsIndication) {
  case MCPS_UNCONFIRMED:
    break;

  case MCPS_CONFIRMED:
    break;

  case MCPS_PROPRIETARY:
    break;

  case MCPS_MULTICAST:
    break;

  default:
    break;
  }

  // Check Multicast
  // Check Port
  // Check Datarate
  // Check FramePending
  // Check Buffer
  // Check BufferSize
  // Check Rssi
  // Check Snr
  // Check RxSlot
  if (certif_running() == true) {
    certif_DownLinkIncrement();
  }

  if (mcpsIndication->RxData == true) {
    switch (mcpsIndication->Port) {
    case CERTIF_PORT:
      certif_rx(mcpsIndication, &JoinParameters);
      break;
    default:

      AppData.Port = mcpsIndication->Port;
      AppData.BuffSize = mcpsIndication->BufferSize;
      AppData.Buff = mcpsIndication->Buffer;

      LoRaMainCallbacks->LORA_RxData(&AppData);
      break;
    }
  }
}

/*!
 * \brief   MLME-Confirm event function
 *
 * \param   [IN] MlmeConfirm - Pointer to the confirm structure,
 *               containing confirm attributes.
 */
static void MlmeConfirm(MlmeConfirm_t* mlmeConfirm)
{
  switch (mlmeConfirm->MlmeRequest) {
  case MLME_JOIN: {
    if (mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {
      // Status is OK, node has joined the network
      LoRaMainCallbacks->LORA_HasJoined();
    } else {
      // Join was not successful. Try to join again
      LORA_Join();
    }
    break;
  }
  case MLME_LINK_CHECK: {
    if (mlmeConfirm->Status == LORAMAC_EVENT_INFO_STATUS_OK) {
      // Check DemodMargin
      // Check NbGateways
      if (certif_running() == true) {
        certif_linkCheck(mlmeConfirm);
      }
    }
    break;
  }
  default:
    break;
  }
}

#define PRINT_KEY(k, n, d) \
  do { PRINTF("%s: %02X", (k), (d)[0]); for (int i = 1; i < (n) ; i++) { PRINTF(":%02X", (d)[i]); } PRINTF(NL); } while (0)

/**
 *  lora Init
 */
void LORA_Init(LoRaMainCallback_t* callbacks, const volatile LoRaParam_t* params, const volatile lora_Configuration_t* config, SPI_HandleTypeDef* hspi)
{
  LoRaParam = params;
  LoRaConfig = config;

  /* init the main call backs*/
  LoRaMainCallbacks = callbacks;

  LoRaSPI = hspi;

  if (LoRaConfig->otaa == ENABLE) {
    PRINTF("OTAA" NL);
    PRINT_KEY("DevEui", 8, LoRaConfig->DevEui);
    PRINT_KEY("AppEui", 8, LoRaConfig->AppEui);
    PRINT_KEY("AppKey", 16, LoRaConfig->AppKey);
  } else {
    PRINTF("ABP" NL);
    PRINT_KEY("DevEui", 8, LoRaConfig->DevEui);
    PRINTF("DevAdd: %08X" NL, LoRaConfig->DevAddr);
    PRINT_KEY("NwkSKey", 16, LoRaConfig->NwkSKey);
    PRINT_KEY("AppSKey", 16, LoRaConfig->AppSKey);
  }
  PRINTF("Port: %d" NL, LoRaConfig->application_port);
  PRINTF("NetID: %d" NL, LoRaConfig->NetID);
  PRINTF("ADR: %s" NL, (LoRaParam->AdrEnable == ENABLE ? "On" : "Off"));
  PRINTF("ConfMsg: %s" NL, (LoRaConfig->ReqAck == ENABLE ? "On" : "Off"));

  LoRaMacPrimitives.MacMcpsConfirm = McpsConfirm;
  LoRaMacPrimitives.MacMcpsIndication = McpsIndication;
  LoRaMacPrimitives.MacMlmeConfirm = MlmeConfirm;
  LoRaMacCallbacks.GetBatteryLevel = LoRaMainCallbacks->BoardGetBatteryLevel;
#if defined( REGION_AS923 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_AS923);
#elif defined( REGION_AU915 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_AU915);
#elif defined( REGION_CN470 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_CN470);
#elif defined( REGION_CN779 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_CN779);
#elif defined( REGION_EU433 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU433);
#elif defined( REGION_IN865 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_IN865);
#elif defined( REGION_EU868 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_EU868);
#elif defined( REGION_KR920 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_KR920);
#elif defined( REGION_US915 )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_US915);
#elif defined( REGION_US915_HYBRID )
  LoRaMacInitialization(LoRaSPI, &LoRaMacPrimitives, &LoRaMacCallbacks, LORAMAC_REGION_US915_HYBRID);
#else
#error "Please define a region in the compiler options."
#endif

  mibReq.Type = MIB_ADR;
  mibReq.Param.AdrEnable = LoRaParam->AdrEnable;
  LoRaMacMibSetRequestConfirm(&mibReq);

  mibReq.Type = MIB_PUBLIC_NETWORK;
  mibReq.Param.EnablePublicNetwork = LoRaParam->EnablePublicNetwork;
  LoRaMacMibSetRequestConfirm(&mibReq);

  mibReq.Type = MIB_DEVICE_CLASS;
  mibReq.Param.Class = CLASS_A;
  LoRaMacMibSetRequestConfirm(&mibReq);

#if defined( REGION_EU868 )
  LoRaMacTestSetDutyCycleOn(LoRaConfig->duty_cycle == ENABLE ? 1 : 0);

#if( USE_SEMTECH_DEFAULT_CHANNEL_LINEUP == 1 )
  LoRaMacChannelAdd(3, (ChannelParams_t)LC4);
  LoRaMacChannelAdd(4, (ChannelParams_t)LC5);
  LoRaMacChannelAdd(5, (ChannelParams_t)LC6);
  LoRaMacChannelAdd(6, (ChannelParams_t)LC7);
  LoRaMacChannelAdd(7, (ChannelParams_t)LC8);
  LoRaMacChannelAdd(8, (ChannelParams_t)LC9);
  LoRaMacChannelAdd(9, (ChannelParams_t)LC10);

  mibReq.Type = MIB_RX2_DEFAULT_CHANNEL;
  mibReq.Param.Rx2DefaultChannel = (Rx2ChannelParams_t) { 869525000, DR_3 };
  LoRaMacMibSetRequestConfirm(&mibReq);

  mibReq.Type = MIB_RX2_CHANNEL;
  mibReq.Param.Rx2Channel = (Rx2ChannelParams_t) { 869525000, DR_3 };
  LoRaMacMibSetRequestConfirm(&mibReq);
#endif

#endif
}

void LORA_Join(void)
{
  MlmeReq_t mlmeReq;

  mlmeReq.Type = MLME_JOIN;
  mlmeReq.Req.Join.DevEui = (const uint8_t*)LoRaConfig->DevEui;
  mlmeReq.Req.Join.AppEui = (const uint8_t*)LoRaConfig->AppEui;
  mlmeReq.Req.Join.AppKey = (const uint8_t*)LoRaConfig->AppKey;
  mlmeReq.Req.Join.NbTrials = LoRaParam->NbTrials;

  JoinParameters = mlmeReq.Req.Join;

  if (LoRaConfig->otaa == ENABLE) {
    LoRaMacMlmeRequest(&mlmeReq);
  } else {
    mibReq.Type = MIB_NET_ID;
    mibReq.Param.NetID = LoRaConfig->NetID;
    LoRaMacMibSetRequestConfirm(&mibReq);

    mibReq.Type = MIB_DEV_ADDR;
    mibReq.Param.DevAddr = LoRaConfig->DevAddr;
    LoRaMacMibSetRequestConfirm(&mibReq);

    mibReq.Type = MIB_NWK_SKEY;
    mibReq.Param.NwkSKey = (const uint8_t*)LoRaConfig->NwkSKey;;
    LoRaMacMibSetRequestConfirm(&mibReq);

    mibReq.Type = MIB_APP_SKEY;
    mibReq.Param.AppSKey = (const uint8_t*)LoRaConfig->AppSKey;;
    LoRaMacMibSetRequestConfirm(&mibReq);

    mibReq.Type = MIB_NETWORK_JOINED;
    mibReq.Param.IsNetworkJoined = true;
    LoRaMacMibSetRequestConfirm(&mibReq);

    LoRaMainCallbacks->LORA_HasJoined();
  }
}

LoraFlagStatus LORA_JoinStatus(void)
{
  MibRequestConfirm_t mibReq;

  mibReq.Type = MIB_NETWORK_JOINED;

  LoRaMacMibGetRequestConfirm(&mibReq);

  if (mibReq.Param.IsNetworkJoined == true) {
    return LORA_SET;
  } else {
    return LORA_RESET;
  }
}

bool LORA_send(lora_AppData_t* AppData, LoraConfirm_t IsTxConfirmed)
{
  McpsReq_t mcpsReq;
  LoRaMacTxInfo_t txInfo;

  /*if certification test are on going, application data is not sent*/
  if (certif_running() == true) {
    return false;
  }

  if (LoRaMacQueryTxPossible(AppData->BuffSize, &txInfo) != LORAMAC_STATUS_OK) {
    // Send empty frame in order to flush MAC commands
    mcpsReq.Type = MCPS_UNCONFIRMED;
    mcpsReq.Req.Unconfirmed.fBuffer = NULL;
    mcpsReq.Req.Unconfirmed.fBufferSize = 0;
    mcpsReq.Req.Unconfirmed.Datarate = LoRaParam->TxDatarate;
  } else {
    if (IsTxConfirmed == LORAWAN_UNCONFIRMED_MSG) {
      mcpsReq.Type = MCPS_UNCONFIRMED;
      mcpsReq.Req.Unconfirmed.fPort = AppData->Port;
      mcpsReq.Req.Unconfirmed.fBufferSize = AppData->BuffSize;
      mcpsReq.Req.Unconfirmed.fBuffer = AppData->Buff;
      mcpsReq.Req.Unconfirmed.Datarate = LoRaParam->TxDatarate;
    } else {
      mcpsReq.Type = MCPS_CONFIRMED;
      mcpsReq.Req.Confirmed.fPort = AppData->Port;
      mcpsReq.Req.Confirmed.fBufferSize = AppData->BuffSize;
      mcpsReq.Req.Confirmed.fBuffer = AppData->Buff;
      mcpsReq.Req.Confirmed.NbTrials = AppData->nbTrials;
      mcpsReq.Req.Confirmed.Datarate = LoRaParam->TxDatarate;
    }
  }
  if (LoRaMacMcpsRequest(&mcpsReq) == LORAMAC_STATUS_OK) {
    return false;
  }
  return true;
}

LoraErrorStatus LORA_RequestClass(DeviceClass_t newClass)
{
  LoraErrorStatus Errorstatus = LORA_SUCCESS;
  MibRequestConfirm_t mibReq;
  DeviceClass_t currentClass;

  mibReq.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm(&mibReq);

  currentClass = mibReq.Param.Class;
  /*attempt to switch only if class update*/
  if (currentClass != newClass) {
    switch (newClass) {
    case CLASS_A: {
      if (currentClass == CLASS_A) {
        mibReq.Param.Class = CLASS_A;
        if (LoRaMacMibSetRequestConfirm(&mibReq) == LORAMAC_STATUS_OK) {
          // switch is instantaneous
          LoRaMainCallbacks->LORA_ConfirmClass(CLASS_A);
        } else {
          Errorstatus = LORA_ERROR;
        }
      }
      break;
    }
    case CLASS_C: {
      if (currentClass != CLASS_A) {
        Errorstatus = LORA_ERROR;
      }
      // switch is instantaneous
      mibReq.Param.Class = CLASS_C;
      if (LoRaMacMibSetRequestConfirm(&mibReq) == LORAMAC_STATUS_OK) {
        LoRaMainCallbacks->LORA_ConfirmClass(CLASS_C);
      } else {
        Errorstatus = LORA_ERROR;
      }
      break;
    }
    default:
      break;
    }
  }
  return Errorstatus;
}

void LORA_GetCurrentClass(DeviceClass_t* currentClass)
{
  MibRequestConfirm_t mibReq;

  mibReq.Type = MIB_DEVICE_CLASS;
  LoRaMacMibGetRequestConfirm(&mibReq);

  *currentClass = mibReq.Param.Class;
}
