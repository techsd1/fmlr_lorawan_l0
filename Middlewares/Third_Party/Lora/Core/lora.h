/******************************************************************************
  * @file    lora.h
  * @author  MCD Application Team
  * @version V1.1.4
  * @date    08-January-2018
  * @brief   lora API to drive the lora state Machine
  ******************************************************************************
  */

#ifndef __LORA_MAIN_H__
#define __LORA_MAIN_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "LoRaMac.h"
#include "region/Region.h"

/* Exported constants --------------------------------------------------------*/
#define LRWAN_MIRO_REVESION (uint32_t) 0x00000007  /* one (hex) digit for Miromico stack addon revision */
#define LRWAN_VERSION  (uint32_t) 0x00001120  /*3 next hex is i_cube release*/

/* Exported types ------------------------------------------------------------*/

typedef enum {
  LORA_RESET = 0,
  LORA_SET = !LORA_RESET
} LoraFlagStatus;

typedef enum {
  LORA_ERROR = -1,
  LORA_SUCCESS = 0
} LoraErrorStatus;

typedef enum {
  LORAWAN_UNCONFIRMED_MSG = 0,
  LORAWAN_CONFIRMED_MSG = !LORAWAN_UNCONFIRMED_MSG
} LoraConfirm_t;

typedef enum {
  LORA_TRUE = 0,
  LORA_FALSE = !LORA_TRUE
} LoraBool_t;

/**
  * Lora Configuration Data Struct
  */
typedef struct {
  FunctionalState otaa;        /*< ENABLE if over the air activation, DISABLE otherwise */
  FunctionalState duty_cycle;  /*< ENABLE if dutycyle is on, DISABLE otherwise */
  uint8_t DevEui[8];           /*< Device EUI */
  uint8_t AppEui[8];           /*< Application EUI */
  uint8_t AppKey[16];          /*< Application Key */
  uint8_t NwkSKey[16];         /*< Network Session Key */
  uint8_t AppSKey[16];         /*< Application Session Key */
  uint32_t DevAddr;            /*< Device address */
  uint32_t NetID;              /*< Current network ID */
  uint8_t application_port;    /*< Application port we will receive to */
  uint32_t TxDutyCycleTime;    /*< Application duty cycle */
  DeviceClass_t class;         /*< Device class */
  FunctionalState ReqAck;      /*< ENABLE if acknowledge is requested */
} lora_Configuration_t;

/*!
 * Application Data structure
 */
typedef struct {
  /*point to the LoRa App data buffer*/
  uint8_t* Buff;
  /*LoRa App data buffer size*/
  uint8_t BuffSize;
  /*Port on which the LoRa App is data is sent/ received*/
  uint8_t Port;
  /* Number of retrials in case of confirmed messages */
  uint8_t nbTrials;
} lora_AppData_t;

/*!
 * TBD: LoRa paramters
 */
typedef struct sLoRaParam {
  /*!
   * @brief Activation state of adaptativeDatarate
   */
  bool AdrEnable;
  /*!
   * @brief Uplink datarate, if AdrEnable is off
   */
  int8_t TxDatarate;
  /*!
   * @brief Enable or disable a public network
   *
   */
  bool EnablePublicNetwork;
  /*!
   * @brief Number of trials for the join request.
   */
  uint8_t NbTrials;

} LoRaParam_t;

/* Lora Main callbacks*/
typedef struct sLoRaMainCallback {
  /*!
   * @brief Get the current battery level
   *
   * @retval value  battery level ( 0: very low, 254: fully charged )
   */
  uint8_t (*BoardGetBatteryLevel)(void);
  /*!
   * \brief Get the current temperature
   *
   * \retval value  temperature in degreeCelcius( q7.8 )
   */
  int16_t (*BoardGetTemperatureLevel)(void);
  /*!
   * @brief Gets the board 64 bits unique ID
   *
   * @param [IN] id Pointer to an array that will contain the Unique ID
   */
  void (*BoardGetUniqueId)(uint8_t* id);
  /*!
  * Returns a pseudo random seed generated using the MCU Unique ID
  *
  * @retval seed Generated pseudo random seed
  */
  uint32_t (*BoardGetRandomSeed)(void);
  /*!
   * @brief Process Rx Data received from Lora network
   *
   * @param [IN] AppData structure
   *
   */
  void (*LORA_RxData)(lora_AppData_t* AppData);

  /*!
  * @brief callback indicating EndNode has jsu joiny
  *
  * @param [IN] None
  */
  void (*LORA_HasJoined)(void);
  /*!
  * @brief Confirms the class change
  *
  * @param [IN] AppData is a buffer to process
  *
  * @param [IN] port is a Application port on wicth Appdata will be sent
  *
  * @param [IN] length is the number of recieved bytes
  */
  void (*LORA_ConfirmClass)(DeviceClass_t Class);
  /*!
   * @brief Call back for confirmed messages
   *
   * @param [IN] frmCnt uplink frame counter
   *
   * @param [IN] ack    true if ack on message received, false otherwise
   */
  void (*LORA_Confirmation)(uint32_t frmCnt, bool ack);
} LoRaMainCallback_t;



/* External variables --------------------------------------------------------*/
/* Exported macros -----------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
/**
 * @brief Lora Initialization
 * @param [IN] LoRaMainCallback_t
 * @param [IN] application parameters
 * @param [IN] configuration parameters
 * @param [IN] hspi SPI device structure
 * @retval none
 */
void LORA_Init(LoRaMainCallback_t* callbacks, const volatile LoRaParam_t* params, const volatile lora_Configuration_t* config, SPI_HandleTypeDef* hspi);

/**
 * @brief run Lora classA state Machine
 * @param [IN] none
 * @retval none
 */
bool LORA_send(lora_AppData_t* AppData, LoraConfirm_t IsTxConfirmed);

/**
 * @brief Join a Lora Network in classA
 * @Note if the device is ABP, this is a pass through function
 * @param [IN] none
 * @retval none
 */
void LORA_Join(void);

/**
 * @brief Check whether the Device is joined to the network
 * @param [IN] none
 * @retval returns LORA_SET if joined
 */
LoraFlagStatus LORA_JoinStatus(void);

/**
 * @brief change Lora Class
 * @Note callback LORA_ConfirmClass informs upper layer that the change has occured
 * @Note Only switch from class A to class B/C OR from  class B/C to class A is allowed
 * @Attention can be called only in LORA_ClassSwitchSlot or LORA_RxData callbacks
 * @param [IN] DeviceClass_t NewClass
 * @retval LoraErrorStatus
 */
LoraErrorStatus LORA_RequestClass(DeviceClass_t newClass);

/**
 * @brief get the current Lora Class
 * @param [IN] DeviceClass_t NewClass
 * @retval None
 */
void LORA_GetCurrentClass(DeviceClass_t* currentClass);

#ifdef __cplusplus
}
#endif

#endif /*__LORA_MAIN_H__*/
