LoRaMAC-Node stack for Miromico FMLR-72-x-STL0 LoRaWAN modules

Port of LoRaMAC-Node stack to Miromico FMLR modules with ST MCU

Project Version:
Drivers: V1.3.0
Middlewares: V1.1.2.7

Toolchain
  STM Atollic TrueStudio