/*
 ******************************************************************************
 * @file    hw_msp.h
 * @author  MCD Application Team
 * @version V1.0.2
 * @date    15-November-2016
 * @brief   Header for driver hw msp module
 ******************************************************************************
 */
#ifndef __HW_POWER_H__
#define __HW_POWER_H__

#ifdef __cplusplus
extern "C" {
#endif

/*low power manager configuration*/
typedef enum {
  LPM_APPLI_Id = (1 << 0),
  LPM_LIB_Id = (1 << 1),
  LPM_RTC_Id = (1 << 2),
  LPM_GPS_Id = (1 << 3),
  LPM_UART_RX_Id = (1 << 4),
  LPM_UART_TX_Id = (1 << 5),
  LPM_LED_Id = (1 << 6),
  LPM_BUZZER_Id = (1 << 7),
} LPM_Id_t;

/*!
* \brief Initializes the HW and enters stope mode
*/
void HW_EnterStopMode(void);

/*!
* \brief Initializes the HW and enters Standby mode
*/
void HW_EnterStandbyMode(void) __attribute__((noreturn));

/*!
 * \brief Exits stop mode and Initializes the HW
 */
void HW_ExitStopMode(void);

/**
  * @brief Enters Low Power Sleep Mode
  * @note ARM exists the function when waking up
  * @param none
  * @retval none
  */
void HW_EnterSleepMode(void);

#ifdef __cplusplus
}
#endif

#endif /* __HW_POWER_H__ */
