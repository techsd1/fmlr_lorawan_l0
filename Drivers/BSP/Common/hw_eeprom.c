/**
  ******************************************************************************
  * @file    hw_eeprom.c
  * @author  Alex Raimondi <raimondi@miromico.ch>
  * @brief   This file provides eeprom memory related functions.
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "hw.h"

bool HW_EEP_write_byte(const uint8_t* dst, const uint8_t data)
{
  return HW_EEP_write_buffer(dst, &data, 1);
}

bool HW_EEP_write_buffer(const uint8_t* dst, const uint8_t* src, uint16_t size)
{
  HAL_StatusTypeDef status = HAL_OK;
  uint32_t dst_addr = (uint32_t)dst;

  HAL_FLASHEx_DATAEEPROM_Unlock();
  for (uint8_t i = 0; i < size; i++) {
    status = HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_BYTE, dst_addr + i, *(src + i));
    if (status != HAL_OK) {
      break;
    }
  }

  HAL_FLASHEx_DATAEEPROM_Lock();
  return status == HAL_OK;
}
