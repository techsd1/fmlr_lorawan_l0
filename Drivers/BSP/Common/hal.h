/******************************************************************************
 * @file    hal.h
 * @author  Alex Raimondi <raimondi@miromico.ch>
 * @brief   Include all STM32 hal drivers
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HAL_H__
#define __HAL_H__

#ifdef __cplusplus
extern "C" {
#endif
/* Includes ------------------------------------------------------------------*/
#if defined(STM32L151xBA)
#include "stm32l1xx_hal.h"
#include "stm32l1xx_hal_conf.h"
#elif defined(STM32L072xx) || defined(STM32L071xx)
#include "stm32l0xx_hal.h"
#include "stm32l0xx_hal_conf.h"
#else
#error Unkonwn controller
#endif

#ifdef __cplusplus
}
#endif

#endif /* __HAL_H__ */
