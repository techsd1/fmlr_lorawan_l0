/******************************************************************************
 * @file    board_conf.h
 * @brief   contains board specific configuration
 ******************************************************************************
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BOARD_CONF_H__
#define __BOARD_CONF_H__

#ifdef __cplusplus
extern "C" {
#endif

/** Define battery limits for current hardware */
#define VDDA_VREFINT_CAL         	((uint32_t) 3000)
#define VDD_BAT                  	((uint32_t) 3000)
#define VDD_MIN                  	1800

// LED definitions
typedef enum {
  LED1 = 0,
  LED_RED = LED1,
  LED2 = 1,
  LED_GREEN = LED2,
  LED3 = 2,
  LED_BLUE = LED3,
} Led_TypeDef;

#define LEDn                              3

#define LED1_PIN                          GPIO_PIN_0
#define LED1_GPIO_PORT                    GPIOH
#define LED1_OFF                          GPIO_PIN_SET

#define LED2_PIN                          GPIO_PIN_1
#define LED2_GPIO_PORT                    GPIOH
#define LED2_OFF                          GPIO_PIN_SET

#define LED3_PIN                          GPIO_PIN_1
#define LED3_GPIO_PORT                    GPIOC
#define LED3_OFF                          GPIO_PIN_SET

#define BUTTON_PIN                        GPIO_PIN_0
#define BUTTON_GPIO_PORT                  GPIOA
#define BUTTON_PRESSED                    GPIO_PIN_RESET

#define BUTTON_CLK_ENABLE()  			  __HAL_RCC_GPIOA_CLK_ENABLE();
#define BUTTON_CLK_DISABLE()			  __HAL_RCC_GPIOA_CLK_DISABLE();

#ifdef __cplusplus
}
#endif

#endif /* __BOARD_CONF_H__ */
